package logica;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Entity;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;

@Entity
public class Odontologo extends Persona implements Serializable{
    
    //private int id_odontologo;
    private String especialidad;
    @OneToMany(mappedBy="odonto")
    private List<Turno> listaTurnos;
    @OneToOne
    private Usuario usUsuario;
    @OneToOne
    private Horario unHorario;

    public Odontologo() {
    }

    public Odontologo(String especialidad, List<Turno> listaTurnos, Usuario usUsuario, Horario unHorario, int id, String dni, String nombre, String apellido, String telefono, String direccion, Date fecha_nac) {
        super(id, dni, nombre, apellido, telefono, direccion, fecha_nac);
        this.especialidad = especialidad;
        this.listaTurnos = listaTurnos;
        this.usUsuario = usUsuario;
        this.unHorario = unHorario;
    }
    
    /*
    public int getId_odontologo() {
        return id_odontologo;
    }
    */
    public String getEspecialidad() {
        return especialidad;
    }

    public List<Turno> getListaTurnos() {
        return listaTurnos;
    }

    public Usuario getUsUsuario() {
        return usUsuario;
    }

    public Horario getUnHorario() {
        return unHorario;
    }
    /*
    public void setId_odontologo(int id_odontologo) {
        this.id_odontologo = id_odontologo;
    }
    */
    public void setEspecialidad(String especialidad) {
        this.especialidad = especialidad;
    }

    public void setListaTurnos(List<Turno> listaTurnos) {
        this.listaTurnos = listaTurnos;
    }

    public void setUsUsuario(Usuario usUsuario) {
        this.usUsuario = usUsuario;
    }

    public void setUnHorario(Horario unHorario) {
        this.unHorario = unHorario;
    }
    
    
}
