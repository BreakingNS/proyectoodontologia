package persistencia;

import logica.Usuario;

public class ControladoraPersistencia {
    HorarioJpaController horarioJpa = new HorarioJpaController();
    OdontologoJpaController odontoJpa = new OdontologoJpaController();
    PacienteJpaController paciJpa = new PacienteJpaController();
    PersonaJpaController persoJpa = new PersonaJpaController();
    ResponsableJpaController respoJpa = new ResponsableJpaController();
    SecretarioJpaController secreJpa = new SecretarioJpaController();
    TurnoJpaController turnoJpa = new TurnoJpaController();
    UsuarioJpaController usuJpa = new UsuarioJpaController();

    public ControladoraPersistencia() {
    }
    
    public void crearUsuario(Usuario usu) {
        usuJpa.create(usu);
    }
    
    
}
